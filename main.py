import sqlite3
conn = sqlite3.connect('hospital.db')
c = conn.cursor()

c.execute("""CREATE TABLE hospital (
            hospital_id integer PRIMARY KEY,
            hospital_name text NOT NULL,
            bed_count integer NOT NULL
         )""")

c.execute("""CREATE TABLE doctor(
        doctor_id integer PRIMARY KEY,
        doctor_name text ,
        hospital_id integer,
        joining_date date,
        speciality text,
       salary integer,
          experience NUll,
         FOREIGN KEY (hospital_id) REFERENCES hospital (hospital_id)
        )""")


hospital_values = "INSERT INTO hospital (hospital_id, hospital_name, bed_count) VALUES (?, ?, ?)"
c.execute(hospital_values, (1, 'Mayo Clinic', 2000))
c.execute(hospital_values, (2, 'Cleveland Clinic', 400))
c.execute(hospital_values, (3 , 'Johns Hopkins',1000 ))
c.execute(hospital_values, (4, 'UCLA Medical Center', 1500 ))
conn.commit()

doctor_values = "INSERT INTO doctor (doctor_id, doctor_name, hospital_id, joining_date, speciality, salary, experience) VALUES (?, ?, ?, ?, ?, ? ,?)"
c.execute(doctor_values, (101, 'David', 1, 2005-2-10, 'Pediatric', 40000, 'NULL'))
c.execute(doctor_values, (102, 'Michael', 1, 2018-7-23, 'Oncologist', 20000, 'NULL'))
c.execute(doctor_values, (103,'Susan', 2, 2016-5-19,'Garnacologist', 25000, 'NULL'))
c.execute(doctor_values, (104, 'Robert', 2, 2017-12-28, 'Pediatric', 28000, 'NULL'))
c.execute(doctor_values, (105, 'Linda', 3, 2004-6-4, 'Garnacologist', 42000, 'NULL'))
c.execute(doctor_values, (106, 'William', 3, 2012-9-11, 'Dermatologist', 30000, 'NULL'))
c.execute(doctor_values, (107, 'Richard', 4, 2014-8-21, 'Garnacologist', 32000, 'NULL'))
c.execute(doctor_values, (108, 'Karen', 4, 2011-10-17, 'Radiologist', 30000, 'NULL'))

conn.commit()

def details(conn,speciality,salary):
    c.execute("SELECT doctor_name FROM doctor WHERE speciality=? AND salary >=?", (speciality,salary))
    print(c.fetchall())

print('enter the speciality')
speciality = input()
print('enter the salary')
salary= int(input())
details(conn,speciality,salary)

def fetch(conn,hospital_id):
    c.execute("SELECT doctor_name FROM doctor WHERE hospital_id=?", (hospital_id))
    print(c.fetchall())
    c.execute("SELECT hospital_name FROM hospital WHERE hospital_id=?", (hospital_id))
    print(c.fetchall())

print('enter the hospital id')
hospital_id = input()
fetch(conn,hospital_id)

conn.close()




